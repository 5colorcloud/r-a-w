Chapter 1. Discovering Blockchain

## Background - The Rising Interest in Distributed Ledger Technologies

Looking back to the last half century of computer technologies and architectures, one may observe a trend of fluctuationbetween the centralization and subsequent decentralization of computing power, storage, infrastructure, protocols, and code.

Over time, Internet and cloud computing architectures enabled global access from a variety of computing devices; whereas mainframes were largely designed to address the needs of large corporations and governments. Even though this 'cloud architecture' is decentralized in terms of hardware, it has given rise to application-level centralization (e.g. Facebook, Twitter, Google, etc).



## Distributed Ledger Technology (DLT)

In summary, distributed ledger technology generally consists of [three basic components](https://intelledger.github.io/introduction.html):

- - A **data model** that captures the current state of the ledger
  - A **language of transactions** that changes the ledger state
  - A **protocol** used to build consensus among participants around which transactions will be accepted, and in what order, by the ledger.



## Blockchains

According to [hyperledger.org](https://hyperledger.org/about),

*"A blockchain is a peer-to-peer distributed ledger forged by consensus, combined with a system for "smart contracts" and other assistive technologies."*



A block commonly consists of four pieces of metadata:

- - The reference to the previous block
  - The proof of work, also known as a nonce
  - The timestamp
  - The Merkle tree root for the transactions included in this block.





