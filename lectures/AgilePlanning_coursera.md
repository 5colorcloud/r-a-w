Agile Planning for Software Products

(https://www.coursera.org/learn/agile-planning-for-software-products/home/welcome)



+++

** Ref

https://bcho.tistory.com/826

https://ko.atlassian.com/agile/project-management/workflow



+++





[Introduction]

- Have the right product
- Ensure it is done right 
- <u>Ensure it is managed right</u>



## Module 1: Introduction to Planning

[week1::Introduction to Planning]

you'll recognize the concepts of <u>user stories from software requirements, sprints from the scrum methodology, and task dependencies from processes</u>.



Whether you manage a team of people directly or through others, you will appreciate three key types of planning involved in software projects:
<u>Release planning, Iteration planning and Risk planning</u>.



In the first Module, Morgan Patzelt will <u>outline uncertainties</u> that projects need to resolve. 
She will also show how to <u>break down work into manageable pieces</u>.

In the second module, Bradley Poulette will explain how to <u>use story points to estimate the size of a requirement</u>. 
He will also explain how <u>estimates relate to velocity as a way to measure the productivity of a development team</u>.

In the third module, Morgan returns to describe how to <u>estimate task times and determine task dependencies</u>.
She will show how to <u>use CPM diagrams and PERT charts to plan work at the task level</u>. This leads to <u>constructing an Iteration plan to map out the tasks to be done within the next sprint</u>.

In the fourth module, 

* Problems or risks that could cause a project to fail
* Anti-patterns (situations with negative consequences for a project and its people)
* Risk assessment and the level of action to take to address those risks
* Constructing a risk plan by mapping out what to do should a risk occur



[week1::Introduction to Planning]

The key to **planning software projects** is breaking the project workload into small, manageable tasks. 
This involves describing and organizing tasks, and assigning them time and work estimates. 

The **release level** refers to the stage where products delivered are ready for the market.
**Release planning** is used to plan the project as a whole. Common release planning techniques include Gantt charts and release plans.

**Iterations** refer to smaller pieces of the project. They usually include a number of requirements that must be finished. A release is made of many iterations.
**Iteration planning** is a technique that involves designing and developing the tasks that will be completed in a certain iteration of
the project. 
Common iteration planning techniques include PERT charts, CPM charts, and iteration plans.



Important Terms

* A **task** is a small, manageable step of a project that must be completed. Tasks are essential to a project — everything in a project can be broken down into tasks.
* A **role** is a duty that a person takes on or plays in some relation to the product. Examples of roles include programmer, tester, designer, client, and product manager.
* A **work product** is an output produced by a task or process.
* A **schedule** is created when the tasks of a project are mapped to a timeline.
* **Milestones** refer to internal checkpoints used to measure progress. They are not time based, but <u>event or action based</u>. Milestones are most commonly used in linear and early iterative processes of a
  project.
  In Agile, milestones are not generally used. <u>Progress is instead measured by working software</u> as opposed to events or actions. Releases and iterations tend to be time based as well, which do not fit with milestones.

These terms have certain dependencies on each other.
.- Roles perform tasks. 
.- Tasks not only create work products, but also might use them. 



After the requirements have been created, they can be prioritized and broken down into **developer tasks** to be completed. 
These developer tasks can be more technology specific than requirements, which should generally be independent of technology. 

Tasks are essential to creating **processes**, as they make up activities, and activities make up phases.

Iteration plans are an Agile practice and used commonly in **Scrum methodology**. Scrum is a popular methodology for
planning and organizing work on software products that follows the Agile philosophy. 



[week1::Uncertainty Space]

**Ends uncertainty** refers to uncertainty about <u>what a product will do</u>.
**Means uncertainty** refers to uncertainty in <u>how a product will be designed and developed</u>.

1. Determine the “what” of a project first, then the “how.” - waterfall process
2. Determine “how” to develop a project, and then create it as you go. - ad hoc development
3. Determine both the “what” and “how” of a project with as much balance between the two as possible.



Development and solutions change as requirements change, so uncertainty will look more like the diagram below, instead of a straight line.


![means end uncertainty development beginsì ëí ì´ë¯¸ì§ ê²ìê²°ê³¼](data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8NDQ8PDxAODw0NDg4ODw4PDQ8PDxAOFREWFhYRFRYYHikgGB0lJxYVIT0hJisrLy8vGB82ODYsNygtLisBCgoKDg0OGhAQGjclICI3Ny8vLTU3LzUtLy0vLS0rLS0vLTcvLS8tLS0tLjU1LS0tLS0tLS0rLS8tLy0tLS0rK//AABEIAKsBJwMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAQIDBQYEB//EAEoQAAEDAgMCBg0JBgUFAAAAAAEAAgMEEQUSIQYxEyJBUZPRFBUWNVRVYXR1gZS10yMyMzRCU5GStAckUnGhwWVygrHSJUNioqX/xAAYAQEBAQEBAAAAAAAAAAAAAAAAAgEEA//EACMRAQEAAgEEAQUBAAAAAAAAAAABAhEDEiExQVEEInHw8RP/2gAMAwEAAhEDEQA/APpiIiAiIglSAoAVwEABXAUAK4CAArAIArAIACsApAUgIACkBSArAIK2U2VrKbIK2SyvZLIKWSyvZLIMdksshCiyDEQoIWQhQQgxEKCFkIVSEGMhUIWUhVIQYyFQhZCFUhBjIWCeYM33tZ7tOZouV6SFhlha62YXtuWXfpleN+JNbo5rg4XuNDrZ3/ED/UF7Vi7Ej04g03cv2g7/AHAPqXgwvHI6qoqYWBwNM8AOPzZW3LHPYeUNe2Rh8rfKFk3PNJL7bRERU0REQEREBSFCsEEgK4CgKwCCQFiqpHMyZQTdzgQGk/Ydlvbdrl1WcBXCylad9VUgAtDn3vYmLKSALG4tpq8ephWCtpJKrFo6bsqrp4m4e+cimkYwukFQ1lzmaeQlarYgYr2xxQ1ktNJTioazg45ZXGCbgInsEWZguzI9jTexu29t9+gpe/7fREn6pizHHXtMmmXuPPjPF/aYPhJ3Hnxni/tMHwl06KlOZ7kHeM8X9pg+Enci7xni/tMHwl0yIOZ7kXeM8X9pg+Enci7xpi/tMHwl0yIOZ7kXeNMX9pg+Enck7xpi/tMHwl0yIOZ7kneNMX9pg+Enci7xpi/tMHwl0yIOZ7kXeNMX9pg+Enci7xni/tMHwl0yIOZ7kXeM8X9pg+Encg7xni/tMHwl0yIOY7kHeM8X9pg+EncefGeL+0wfCXTog5juOPjPF/aYPhKO44+M8X9pg+EuoRBy3cb/AIli/tMHwk7jP8Sxb2mD4S6leDF6x0MYyAOmlcIomncZHcp8g1PqWW6m2W6aXuL/AMSxb2mD4Sg7Fjxli3tMHwls48BhIvNnmlPzpHySAk8tgDZo8gVxs9ScsIP+Z8jv9ys3l8M3XD7X7ORU1M+pGI100tMOEFJPX2iqAP8AslseQ5nXsHa2NrgjRafEJKGZsdNSy04mjljo2vhncH0rZXhpc3g3Bx1DRa9s1r7iF9RqNn6KWJ0T6aB0T8uZnBts7K4OAPOLgabjyrjIcLp5cUxVr4YnNp63D3wjI0cE9lBTlro7fNtYbly8/wBJ/tyYZ3Kzp9T3+f329+PluONx15baGDsanZGx0snBBjc8r3zyuGYZnOcblx3/ANrLBJPNxsoJ42UAx2AuXgcmosGH1rZqF1XG328LN+2OmcXMa51wXDNYixAJuAR5NAiyIqihERBKsFUK4QWCuFUK4QWAVgFAVwg0ezg/e8X9IRe76VZqfv8At9ESfqmLFs4P3vF/SEXu+lWaDv8At9ESfq2IOqREQEREBERAREQEREBERAREKDFUVDIm5pHtY0fac4NH4leE4/R+ERfmusGGwire6qkGZuZzaZjhdrIgbcJY/ada9+ay3AYByKN5XvE7t8Nf2/o/CIvzKDj9H4RF6nXWxyDmH4IGDmH4Lfu/f6d2sOPQn6ITTHmigkP9SAP6pRU0sk3ZE7QxzWlsMIcHcE0/Oc47i4+TcFtLKU6b7NfIiIqULhKHvrjXnVF7vgXdrhaHvrjXnVF7vgQbVERAREQERSgkK4VQrhBYK4VQrhBYK4VQrhBo9m/reL+kIvd9Ks0Pf9voiT9WxYtm/rmMekIvd9IssPf9voiT9WxB1KIiAiIgIiICIiAiKHOABJNgBck7gEEotQzEKio1pomCL7M07nNDxztYBe3lNlfJX/x0nRS/8lPV8J6m0Wux6csp3NZ9LMRBH/nfxb+rU+pUtX89GfVMP7qaagkdK2aoe174wREyNpbHHfQu1NyeS6y22aLd9nupYBFGyNujWNawfyAssqIrUIiICIiAiIgLhaDvrjXndF7vgXdLhqDvrjXndF7vgQbVQpKhAREQeevrY6aMySFwYHMZxIpJXlz3BrWtYwFziSQNAvPU45SRMmfJUQtbTAOmBkbnjvewc3eCbEBtrkiytjeFsrad0Ehc1jnRvu1sTjdjw8aSNc0i7RoQV4a7ZaGdrgZahjnmQ52GHMM4kD7BzC3USvbu0vpYgFBthiEOV7uFYRE4NkyuzljibBrgNQddyqzGaXX94gaWxvlc18rWObEw2dIWusQ0c+5a6ow+np2TNknmY2vks36O0UtnyZow1mUHQuLng3IFydF5YqCgkyRNq3mR7eI8Pizue27M44mUuzOva1r8ltEHQNxelIv2TTWAjN+HjtaQXjN7/auCOe6rTY9SSxsfHPE8SxslY1r28I6N0gjD8h1tmNt2/RadlJQU8/DPqy+ZxZPxnU7i90Rjic4ZGAkudwYLR9q1gFfC8ApZ2wyxVVRNDCxkUPHiMeRkjXtI4gJFmsFxo5tjqTmQbgbQUWdrOyqcufw1rTMLfkmtdICQbAgPabHkK2cMjXjM1zXNuRdpBFwSCLjmII9S55+y0QjLY3vJMMkBbKWOikjfTthLJAG3tZjDpY3G+xIW5wmiFLTQwBzniCKOLO75zy1oBe7ynU+tBrtm/reMekYvd9IskXf9noiT9WxY9m/rmMekYvd9IssXf9noiT9WxB1CIqPla0XcQ0c5IAQXReXthB97F0rOtO2MH3sPSs61m4zcepF5e2MH3sPSs607Ywfew9KzrTcNx6kXl7Ywfew9KzrTtjB97D0rOtNw3HqWmxytjko5xFJG88SNwZI1xGd4aQbHTeVy37T8T4RtNSskBhqDO+cMkaQ9sYYGwvt9l2cuI5eDsdLg8BVWpmGpiayOakaZonhoFjHxw023sOUAt5QvDl55L0/Ls4fosubiucr7/GwNAAFgBYDmA5FZcbiu2D6WqmjLYHsjhik4DM5k8efIOGldcgRAvsTl0sTruWCp/aCY4y7sR2ZopSQ6pjbH8vTmZnytsmoZIy97Zw0XAdcdDkdyi4g7dOY6SN8UDnx9km/ZHAnKyrlia9zC12WOzADJf52llRv7QCZIB2M0xzcDd7KrObvdRtOQcHZ4vWxWNxmAJ00BDukXDN27llgbJFTwl0kEkjT2ZwjA/JO6MDKzjAmGx+aRc77a+jFtrZYJsLa0QPjr4TLII3CUu1iA4B2dpePlCbhjrgAnI27gHYovnlT+0kiFjm08YMtLUztkFW2Rl46eqlHB2YOEA7Fs6+XKZWb7r3O28yySMdAxojq204e6qABjNTWU5ky5MxdejfxGhxs9uoAcQHaoiICIiAuGw/vrjfndF7vgXcrhsP764353Re74EG1KhSVCAiIgKQoUhB4cZwoVjAwvdGLTNJaONlkgkiOU/ZIzhwOvzfKtfSbGwRABr5LNkMjQQwhpL6d+mmmtPf8AnI/yLoQrhBz9JsmyNrx2RO8yQ1ML3uy53CZkLS7MNQRwIOhtxj5Lb3D6MQB4FrSSvks1oY1oNg1gHMA1o9XJuWYK7UGQK4VArhBpNm/rmMekYvd9Kskff9noiT9WxYtm/rmMekYvd9Issff9noiT9WxBs66ollm7GgdkLWh881gTG0/Na0H7Rsd+4K0WBUwN3xiV/K+cmZx8vGVdnxnjkmO+onlk/wBAORg/BoW1USb71Mm+9eQYXTfcQdDH1J2sp/uIOhZ1L1oq1G6jydrKf7iDoWdSdrKf7iDoWdS9aJqGo8nayn+4g6FnUnayn+4g6FnUvWiahqNNjOzFJWQ8E6JsdnB7JYWsjlikAID2Otv1IsQQQSCCDZaTDf2eQRTMkmnmqWxOa9kTmRMjL2kFrpMou+xANrgc4K7RFlwxt3p6Y8mWONxxupUFo5hqmUcwUoqQxVFMyVpY9rXNNrgjTQgj+oBWTKOYKUQQGjmCBo5hpu0Uog8suHQvmjmcxplhzcG434lw5pIG69nOF99nOHKV6co5h+ClEBERAREQFw2H99cb87ovd8C7lcNh/fXG/O6L3fAg2pUKSoQEREBSFClBcK4WMK4QZArhYwrhBkCuFQK4QaTZv65jHpGL3fSLHWTcHjD3/wAGB1DvwqWlZNm/reMekIvd9KvBj5/6lIBvdgzo/wA9bG3+6zLtKy+HYYPDwdLAzlbDGD/PKLr2KGiwA5hZSkmo2CIi0EREBERAREQEREBERAREQEREBERAREQFw2H99cb87ovd8C7lcNh/fXG/O6L3fAg2pUKSoQEREBERBYLIFiCyBBkCuFjCuEGQK4WMK4QaXZv63jHpCL3fSrxYzrjMTf46GFv/ANCM/wBls8Eo5IqnEnvblZU1kcsRu05oxR08ZOh04zHDXmWsxPv/AEjeekb/AOtUD/ZTn4Tl4d2EQIqUIiICIiAiIgIiICIiAiIgIiICIiAiIgIiIC4Wg764153Re74F3S4Wg764153Re74EG2UKVCAiIgIiIJCuFjVggyhWCxhXBQZAV5Kymke8Oa4Boa0Ec9iT/deoFWBU5YyzVXhncLuNYzD5teM0fMOfMS42/nzblqZmFu0GHNOpbhzg7l1bKF1gK57arBqSd8U88Uj5o2SRsfHU1MDmssX2+Se3S45VFwxxm15ZZ832u2RfNZMBoWl/EqeIJrDtrihc4sMlj9NoPk/xKtUYRhkLWmSDF3F7pgBT1GOVOUMkLOPwTzlOm4+VVjnjldRmfDnhN2PpCL5h2Lg/g20HR7SdansXB/BtoOj2k61byfTkXzLsXB/BtoOj2k61PYmD+C7QdHtJ1oPpiL5n2Jg3gu0HR7SdadiYN4LtB0e0nWg+mIvmnYmDeC7Q9HtJ1p2Jg3gu0HR7SdaD6Wi+Z9iYN4LtB0e0nWo7EwfwbaDo9pOtB9NRfMuxcH8G2g6PaTrUGlwfwbaDo9pOtB9ORfMDS4R4NtB0e0fWqmmwjwbH/wAm0fWg+oovlppsI8Hx/wDJtH1qpp8J8Hx/8m0XWg+qIvlJp8J8Hx/8m0XWo4DCvB8f/JtF1oPq6L4liGI4XBWU1OabGiyqDwHmfG45GvBFgInvDpBqbltyLbjddNV7M0UZtaq1Y5wviuIjUOaPvd2pPqWW6Zbp9HXCUPfXGvO6L3fAtSMCpMzW5arjODbjFcStq6wP0u42efUFvsJweCjEgga8cM8Pkc+eaZznBoaCXSOJ3AD1LMcpfBLL4e5ERU0REQEREBSFCIMgKsCsYKuCgyAq4KxAq4KDICrLGCrAoLgDmHLyc6yBYwVYFBkBVgViBVgUGQFWusQKm6DJdY6upbDFJK++SKN8j7C5ytaXGw5dykFYquBs0UkT7lk0b4ngEgljmlpseTQoNbh211BUgGOobZ0ogbmBZmlyxHKL8xmjbr9pwbvICzjaKj0/eIxma9wzXbYMz5r3HFtwUm+3zCvNUbMUkkwmcw8KHueHB5Fi5kTDbm+giNxY3b5TetLstSQgBjZBl3ESuBBvKbi1rfTybuccwQe6pxuCKNkrnOMcrXuYQx1yGC50Oo9aq3HaQnKJ2F3C8BbjX4W5GW1v/Fwvu0Oui00kGHNZHRuMrW0s+RgvLxnyyRtLLt0LC6pY3LoBuFg3SYGYWSyeOqj35myMrAA5t5pC24OrT8qSOUM8iD3zbU0TYw8TCQOy5RG1z3EOexgcABe3yjDfmcCL3F8s2O07HlpkHEL2yH+BzcuhB1N8wsQCtHhNHhpJoqfh2ljHM1bNGWiGSFwIc8DNrwZDtQ4NJudSdlHszSte94a/NJIZXEyE3eXh1/xaPwQZGbS0ZLvlgAwsGdzXBjy6AzAMNuMcrXG2/ilbQlaGq2YheI2tcWxtqKWoe115MxgJLA03GQnQE2N2gi2txuyUAlVJQlVJQCVQlCVUlBidAwyCUsYZWsdG2QtGdrCQS0O3gGw05bDmUuAPMpJVSUFSPIoUqEBERAREQEUoghFKICsCqqQguCrgrGFYIMgKsCsYVggyAqwKxhWCDICpBVApCC4Km6oFKC91N1REF7qLqqINLV7MUs8/DShz3iYTR3yfJOvCXBhtcBxgF9b2e4biLeOfYijkY1kjqiTJm4zpWhzriJozZWgHK2JrBp81z73LiV0qhB4o8MYyo4cF2YmocW2YGl8vBXdoLmwiAF/4j5Le0lQVBQCVBKFVQQSqkqVUoIJVSVJVSgqSqlWcqoIRSiCEUoghFKIP/9k=)



[week1::Work Breakdown Structure (WBS)]

In project management, the technique of **work breakdown structure (WBS)** creates a visual representation of the process of breaking down large project work products into smaller task work products.
<u>Tasks should be broken down until each is small and clear enough to be assigned to a single person</u>. 
WBS is therefore also a hierarchical representation of project tasks and deliverables. It can take on the form of a list or other visual representation. 
<u>Dates and order of execution are not included in a WBS</u>. 



The acronym SMART is sometimes applied to determine if a task or work product is a good size. 

* Specific (the task should be clear and should not overlap with other tasks)
* Measurable (the task should have a definite way of being marked as finished or done)
* Achievable (one person should be able to do the task)
* Relevant (the task should be within scope of the project)
* Time boxed (the task can fit within a certain time frame)



WBS can be organized functionally or by phase. Common steps included are:

* Project initiation (when a new project is started and objectives, scope, purpose, and
  products are decided)
* Hardware and equipment acquisition (determining what hardware or equipment is
  needed to create the product, and obtaining those)
* Development (designing and creating the product)
* Testing (determining the performance and reliability of the product to identify and fix problems)
* Implementation (when the product is executed for outsiders of the project to be able to access and use) 



Creating a Work Breakdown Structure

Work breakdown structures are also not limited to product management concerns surrounding the <u>Design and Implementation phase</u>. 
They might also include <u>Specification phase activities, Verification and Validation phase activities, promotion, arranging technical support, gathering analytics, setting up servers</u>, etc.



Uses of Work Breakdown Structures

The most common use of a WBS is to <u>determine tasks</u> for the project based on the work products displayed. 

Work breakdown structures can be used to help <u>assess how long it will take a development team to complete a project</u>. It is easier to estimate how long small tasks will take to finish, and then to add those times together to determine a project estimate, than to estimate the project as a whole. 

WBS may also be used to <u>determine potential risks</u> the project will encounter. Once work products have been identified through a WBS, risks can be determined for each of those work products. 

Finally, a work breakdown structure can also be used to <u>demonstrate a product</u> to a client in a way that allows the client to look at features individually. 



[week1::Estimates, Targets, and Commitments]

An **estimate** is a guess of the time it will take the development team or worker to complete a task. 

It is important to understand that although estimates are sometimes presented as ranges, <u>they are not negotiable</u>. This means that an estimate should only take into account previous work. 
It should not include padded or extra time or be affected by what stakeholders or managers want the estimate to be.



![Cone of Uncertainty Principle.ì ëí ì´ë¯¸ì§ ê²ìê²°ê³¼](http://www.agilenutshell.com/assets/definitions/cone-of-uncertainty.png)



Approaches for creating an estimate include:
.- Bottom-up
.- Analogy
.- Experts
.- Using an estimate formula



A **target** is a specific point in the schedule for the project or part of the project to meet. 
It is almost an ideal deadline, and is usually set externally to the development team. Like estimates, <u>targets are not negotiated</u>. 

Examples of targets are ends of a sprint or the release date of a product.



**Commitments** are what you agree to deliver. Commitments are where negotiations can happen. Based on both estimates and targets, a commitment can be negotiated. 
In order to determine a commitment, best practice suggests that estimates should be discussed with the development team first, so accurate numbers can be determined. There should also be discussions with the client in order to determine target dates. 
Then, <u>based on these discussions, commitments can be determined with both the development team and the client</u>. 



**Estimate, Target, and Commitment Example**
It is therefore important that estimates, targets, and commitments are made clear and separate for both the client and the development team in their discussions. If this can be done, it is more likely that the project and its target dates can run as planned, without the scope becoming too large, as it ensures that target dates can actually be met. Again, drawing from the above example, understanding that <u>1,000 hours is an estimate, and the target is 600 hours helped create a commitment of 500 hours.</u> 
This commitment helped narrow the scope of the project to stay within the target.



## Module 2: Estimation



