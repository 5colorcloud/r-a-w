### [Effective Problem-Solving and Decision-Making](https://www.coursera.org/learn/problem-solving/home/welcome)



1. Introduction to Problem Solving

#### 1.1 Decision Making and Problem Solving in Organizations

* Know your purpose.

* Understand your biases.

* Consider different consequences.



**Problem**, problem is a gap or it's some kind of a change. 
There's something not right in the way that we usually understand things.

Problem solving, process of working through all the details of a problem to reach some kind of solution. 
So it's a process, <u>problem solving is a process</u> and we do that in a couple of different ways.

