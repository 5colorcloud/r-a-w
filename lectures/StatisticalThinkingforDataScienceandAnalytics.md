



---

Statistical Thinking for Data Science and Analytics



[Week 1: Introduction to Data Science]  



One of the big challenges, I think, for data science
is how to use that kind of observational data
to answer questions about the world.
This is a type of question that traditional statistics
and traditional machine learning can't answer.



[Week 2: Statistics and Probability I]

2.1 Lecture: Statistical Thinking for Data Science

Statistical Thinking for Data Science



![image-20190424123642058](/Users/future_riss/Library/Application Support/typora-user-images/image-20190424123642058.png)



Numerical Data 1 Simple Visualization (Part 1)

Data: number with context



> Percentiles
>
> Confidence Intervals
>
> Sampling distribution

<https://drhongdatanote.tistory.com/49>



**Sampling distribution** by simulation,
by experiment, or sometimes we describe
it using mathematical model.

— random sample : 확률표본

. identical distribution : 동일분포

. independent distribution : 독립분포

— Random variable : 확률변수

— Probability distribution : 확률 분포

— Sampling distribution : 표본분포. 통계량의 확률 분포


![img](https://t1.daumcdn.net/cfile/tistory/998A3C335A214C391B)



**Central Limit Theorem** - 중심극한정리 
모집단(분석의 대상이 되는 집단)에서 K개씩의 샘플을 N회 <u>복원추출</u>한다고 할 때 K가 약 30이상의 너무 적지 않은 개수이고 시행횟수 N이 많아 질수록 K의 평균들이 정규분포에 점점 가까워진다.

“30명씩 100회를 추출하지 않고 한꺼번에 3000명을 한 번만 추출한 것의 평균이 모집단의 평균과 일치하는가?”
매우 가깝긴 하겠지만 그렇지 않다. 평균들의 평균이 모집단의 평균에 수렴하는 것이다. 위에서 말한 보정이 필요한 이유이다.

<http://intothedata.com/02.scholar_category/statistics/central_limit_theorem/>

<https://dermabae.tistory.com/146>







++

